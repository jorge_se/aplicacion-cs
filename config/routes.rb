Rails.application.routes.draw do

  devise_for :admins, controllers: {sessions: 'admins/sessions'}
  devise_for :users, controllers: {sessions: 'users/sessions'}
 # devise_for :users
  get 'order_details/new'

  get 'order_details/show'

  get 'order_details/destroy'

  get 'payment/send'

  get 'payment/receive'

  resources :posts do
    resources :comments
  end
  root 'posts#index'
  #me mandara al controlador post a la accion index(es un metodo dentro de la clase)
  get '/jorge',to:'posts#new'
  #el metodo para esa ruta, lo que queremos q teclee el usuario y lo voy a mandar a mi controlador
  #al metodo que deseo que muestre
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
